{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
import           Control.Monad.IO.Class  (liftIO)
import           Database.Persist
import           Database.Persist.Sqlite
import           Database.Persist.TH


share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Person
    name String
    age Int Maybe
    deriving Show
BlogPost
    title String
    authorId PersonId
    deriving Show
|]

-- data Person = Person
--     { personName :: !String
--     , personAge :: !Int
--     }
--   deriving Show

-- type PersonId = Key Person

-- instance PersistEntity Person where
--     newtype Key Person = PersonKey (BackendKey SqlBackend)
--         deriving (PersistField, Show, Eq, Read, Ord)
--     -- A Generalized Algebraic Datatype (GADT).
--     -- This gives us a type-safe approach to matching fields with
--     -- their datatypes.
--     data EntityField Person typ where
--         PersonId   :: EntityField Person PersonId
--         PersonName :: Ent ityFie
--    ld Person String
    --     PersonAge  :: EntityField Person Int

    -- data Unique Person
    -- type PersistEntityBackend Person = SqlBackend

    -- toPersistFields (Person name age) =
    --     [ SomePersistField name
    --     , SomePersistField age
    --     ]

    -- fromPersistValues [nameValue, ageValue] = Person
    --     <$> fromPersistValue nameValue
    --     <*> fromPersistValue ageValue
    -- fromPersistValues _ = Left "Invalid fromPersistValues input"

    -- -- Information on each field, used internally to generate SQL statements
    -- persistFieldDef PersonId = FieldDef
    --     (HaskellName "Id")
    --     (DBName "id")
    --     (FTTypeCon Nothing "PersonId")
    --     SqlInt64
    --     []
    --     True
    --     NoReference
    -- persistFieldDef PersonName = FieldDef
    --     (HaskellName "name")
    --     (DBName "name")
    --     (FTTypeCon Nothing "String")
    --     SqlString
    --     []
    --     True
    --     NoReference
    -- persistFieldDef PersonAge = FieldDef
    --     (HaskellName "age")
    --     (DBName "age")
    --     (FTTypeCon Nothing "Int")
    --     SqlInt64
    --     []
    --     True
    --     NoReference

main :: IO ()
main = runSqlite ":memory:" $ do
    runMigration migrateAll

    johnId <- insert $ Person "John Doe" $ Just 35
    janeId <- insert $ Person "Jane Doe" Nothing

    insert $ BlogPost "My fr1st p0st" johnId
    insert $ BlogPost "One more for good measure" johnId

    oneJohnPost <- selectList [BlogPostAuthorId ==. johnId] [LimitTo 1]
    liftIO $ print (oneJohnPost :: [Entity BlogPost])

    john <- get johnId
    liftIO $ print (john :: Maybe Person)

    delete janeId
    deleteWhere [BlogPostAuthorId ==. johnId]

    people <- selectList [PersonAge >. (Just 25), PersonAge <=. (Just 30)] []
    liftIO $ print people

resultsForPage pageNumber = do
    let resultsPerPage = 10
    selectList
        [ PersonAge >=. (Just 18)
        ]
        [ Desc PersonAge
        , Asc PersonName
        , LimitTo resultsPerPage
        , OffsetBy $ (pageNumber - 1) * resultsPerPage
        ]

putPersons :: SqlPersist m ()
putPersons = do
  people <- select $
              from $ \person -> do
              return person
  liftIO $ mapM_ (putStrLn . personName . entityVal) people
