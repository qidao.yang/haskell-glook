{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeOperators         #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DeriveGeneric          #-}

module Handlers.Users where

import           Database.Persist.Sqlite
import           GHC.Generics
import           GHC.Int                    (Int64)
import           Servant                    hiding (Handler)
import           Data.Time
import           Control.Monad.IO.Class

import           AppM
import           Database
import           Model
import           Data.Swagger(ToSchema)
import  Yesod

-- glook api

data PutUserR = PutUserR { code :: String, app_id :: String} deriving(Show, Generic)
instance ToSchema PutUserR
instance ToJSON PutUserR
instance FromJSON PutUserR

type PutUser = "users" :>  ReqBody '[JSON] PutUserR :> Put '[PlainText] String
putUser :: PutUserR -> AppM String
putUser (PutUserR code' app_id') =  runDb $ do
  time <- liftIO  getCurrentTime
  codem <- return $ if (code' /= "") then Just code' else Nothing
  app_idm <- return $ Just app_id'
  key <- insert (toEntity $ UserModel Nothing Nothing Nothing app_idm Nothing Nothing codem Nothing (Just time) (Just time) :: User) --(toEntity model {created=time, updated=time}:: User)
  return $ show $ fromSqlKey key

data GetUserR = GetUserR { nickname :: String, groups :: [String], income :: Rational, balance :: Rational}
type GetUser = "users" :> Capture "id" Int64 :>  Get '[JSON] UserModel
getUser :: Int64 -> AppM UserModel
getUser i = runDb $ do
    entity <- selectFirst [UserId <-. [toSqlKey i :: Key User]] []
    case entity of
      (Just e) -> return $ fromEntity e
      Nothing    -> throwError err404  { errBody = "user not found" }
