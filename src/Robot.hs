{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Demo.Dialogue where
import Network.AMQP

import           Control.Monad (forM_)
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.ByteString as B
import           Data.Monoid ((<>))
import qualified Data.Text as DT
import qualified Data.Text.Lazy.Encoding as DT
import qualified Data.Text.Lazy.IO as TL
import           System.Environment (getArgs)
import  GHC.Generics
import  Data.Aeson

data MSG = MSG { command::String, body::Value}
   deriving(Show, Generic)
instance FromJSON MSG
-- [rmq](https://rabbitmq.gemii.cc/)      gbotdev:XQLRFv2mTu4jbVev

-- GPET_MQ_CONFIG = {
--         'server': 'amqp://gbotdev:XQLRFv2mTu4jbVev@gbotdev.gemii.cc:2100/gbotdev',
--         'exchange_type': 'topic',
--         'exchange_name': 'gemii.gbot.topic',
--     }
logsExchange = "gemii.gbot.topic"



main :: IO ()
main = do
     conn       <- openConnection''  $ fromURI "amqp://gbotdev:XQLRFv2mTu4jbVev@gbotdev.gemii.cc:2100/gbotdev"
     ch         <- openChannel conn
     severities <- getArgs

     -- declareExchange ch newExchange {exchangeName    = logsExchange,
     --                                 exchangeType    = "topic",
     --                                 exchangeDurable = True}
     (q, _, _) <- declareQueue ch newQueue {queueName       = "queue.bot.common.glook-h"}
                                            -- queueAutoDelete = True,
                                            -- queueDurable    = True}
     -- forM_ severities (bindQueue ch "queue.bot.msg.pet" logsExchange . DT.pack)


     BL.putStrLn " [*] Waiting for messages. To exit press CTRL+C"
     _ <- consumeMsgs ch q Ack deliveryHandler

     -- waits for keypresses
     getLine
     closeConnection conn

out content = do
  BL.writeFile "private_message.json" content
  print "[O] GOT IT!"

encoded content = do
   TL.writeFile  "private_message.json" $ DT.decodeUtf8 content


deliveryHandler :: (Message, Envelope) -> IO ()
deliveryHandler (msg, metadata) = do
  case eitherDecodeStrict' $ BL.toStrict body of
    Left e -> error e
    Right r -> if (command r == "private_message") then encoded  body else return ()

  where
    body = msgBody msg

test = do
  print "中文"
  --print "\u4e2d\u6587\u5b57\u7b26"
