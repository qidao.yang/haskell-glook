{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Api where

import           Control.Lens
import           Data.Swagger
import           Servant                         hiding (Handler)
import           Servant.Swagger

import           AppM
import           Database
import           Handlers

readerServer :: Config -> Server CombinedAPI
readerServer cfg = hoistServer (Proxy :: Proxy CombinedAPI) (makeNat cfg) server

type CarAPI = AddCar
         :<|> UpdateCar
         :<|> GetCars
         :<|> GetCar
         :<|> DeleteCar

carApi :: ServerT CarAPI AppM
carApi = addCar
    :<|> updateCar
    :<|> getCars
    :<|> getCarModel
    :<|> deleteCar
--

type PersonAPI = AddPerson
            :<|> UpdatePerson
            :<|> GetPerson
            :<|> GetPersons
            :<|> GetPersonCars

personApi :: ServerT PersonAPI AppM
personApi = addPerson
       :<|> updatePerson
       :<|> getPerson
       :<|> getPersons
       :<|> getPersonCars
--

type JobAPI = AddJob
         :<|> UpdateJob
         :<|> GetJobs
         :<|> GetJob

jobApi :: ServerT JobAPI AppM
jobApi = addJob
    :<|> updateJob
    :<|> getJobs
    :<|> getJob
--

--glook api


--
type GlookAPI = GetUser
            :<|> PutUser

glookApi :: ServerT GlookAPI AppM
glookApi = getUser
  :<|> putUser

type CombinedAPI = CarAPI
             :<|> PersonAPI
             :<|> JobAPI
             :<|> GlookAPI
             :<|> WithHeader
             :<|> ReturnHeader
             :<|> CaseError

server :: ServerT CombinedAPI AppM
server = carApi :<|> personApi :<|> jobApi
    :<|> glookApi
    :<|> withHeader
    :<|> responseHeader
    :<|> caseError

-- Swagger Docs
getSwagger :: Swagger
getSwagger = toSwagger (Proxy :: Proxy CombinedAPI)
  & basePath .~ Just "/api"
  & info.title   .~ "Glook API"
  & info.version .~ "0.0.1-SNAPSHOT"
  & applyTags [Tag "API Controller" (Just "API Controller Name") Nothing]
