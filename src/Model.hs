{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}

module Model where

import           Data.Maybe
import           Data.Time
import           Data.Swagger
import           Database
import           Database.Persist.Sql
import           GHC.Generics
import           GHC.Int
import           Yesod

class ApiModel a e | a -> e where
  toEntity :: a -> e
  toUpdate :: a -> [Update e]
  fromEntity :: Entity e -> a

-- Car
data CarModel = CarModel { id :: Maybe Int64, make :: String  } deriving (Generic)
instance ToSchema CarModel
instance ToJSON CarModel
instance FromJSON CarModel
instance ApiModel CarModel Car where
  toEntity m = Car { carMake = make m }
  toUpdate (CarModel _ m) = [CarMake =. m]
  fromEntity e = let value = entityVal e
                 in  CarModel { id = Just  (fromSqlKey (entityKey e)), make = carMake value }


-- Person
data PersonModel = PersonModel { id      :: Maybe Int64
                               , name    :: String
                               , address :: Maybe String
                               , carId   :: Maybe Int64
                               } deriving Generic

instance ToSchema PersonModel
instance ToJSON PersonModel
instance FromJSON PersonModel
instance ApiModel PersonModel Person where
  toEntity (PersonModel _ name' address' carId') = Person name' address' (toSqlKey <$> carId')
  toUpdate (PersonModel _ name' address' carId') = [ PersonName =. name'
                                                   , PersonAddress =. address'
                                                   , PersonCarId =. toSqlKey <$> carId'
                                                   ]
  fromEntity e = let value = entityVal e
                 in  PersonModel { id = Just $ fromSqlKey (entityKey e)
                                 , name = personName value
                                 , address = personAddress value
                                 , carId = fromSqlKey <$> personCarId value
                                 }
-- Job
data JobModel = JobModel { id          :: Maybe Int64
                         , title       :: String
                         , description :: Maybe String
                         } deriving Generic

instance ToSchema JobModel
instance ToJSON JobModel
instance FromJSON JobModel
instance ApiModel JobModel Job where
  toEntity (JobModel _ title' description') = Job title' description'
  toUpdate (JobModel _ title' description') = [ JobTitle =. title'
                                              , JobDescription =. description'
                                              ]
  fromEntity e = let value = entityVal e
                 in  JobModel { id = Just $ fromSqlKey (entityKey e)
                              , title = jobTitle value
                              , description = jobDescription value
                              }

-- Glook

data UserModel = UserModel { id :: Maybe Int64
                           , union_id :: Maybe String
                           , open_id :: Maybe String
                           , app_id :: Maybe String
                           , head_url :: Maybe String
                           , nickname :: Maybe String
                           , code :: Maybe String
                           , introduction :: Maybe String
                           , created :: Maybe UTCTime
                           , updated :: Maybe UTCTime
                           }deriving Generic

instance ToSchema UserModel
instance ToJSON UserModel
instance FromJSON UserModel
instance ApiModel UserModel User where
  toEntity (UserModel id' union_id' open_id' app_id' head_url' nickname' code' introduction' created' updated') = User union_id' open_id' app_id' head_url' nickname' code' introduction' (fromJust created') (fromJust updated')
  toUpdate (UserModel {code = code'
                      ,updated = updated'}) = [UserCode =. code', UserUpdate_date =. fromJust updated' ]
  fromEntity e = let value = entityVal e
                 in UserModel { id = Just $ fromSqlKey (entityKey e)
                              , union_id =  userUnion_id value
                              , open_id = userOpen_id value
                              , app_id = userApp_id value
                              , head_url = userHead_url value
                              , nickname = userNickname value
                              , code = userCode value
                              , introduction = userIntroduction value
                              , created = Just $ userCreate_date value
                              , updated = Just $ userUpdate_date value
                              }

data GroupModel = GroupModel { id :: Maybe Int64
                             , user_id :: Maybe Int64
                             , code :: Maybe String
                             , lable :: Maybe Int
                             , introduction :: Maybe String
                             , hear_url :: Maybe String
                             , price :: Maybe Double
                             , created :: Maybe UTCTime
                             , updated :: Maybe UTCTime}
  deriving Generic

instance ToSchema GroupModel
-- instance ApiModel GroupModel Group where
--   toEntity (GroupModel id' user_id' code' lable' introduction' hear_url' price' created' updated') = Group (toSqlKey $ fromJust user_id') code' lable' introduction' hear_url' (fmap toRational price') (fromJust created') (fromJust updated')
--   toUpdate (GroupModel )
