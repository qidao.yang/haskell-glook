#!/usr/bin/env stack
-- stack --resolver lts-8.12 script
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module Demo.JSON.Lens where
import Control.Lens
import Data.Aeson.Lens
import Data.Aeson
import Data.Text (Text)
import qualified Data.ByteString as B
import Data.Text.Encoding as E
import GHC.Generics

data MSG = MSG { command::String, body::Value}
   deriving(Show, Generic)
instance FromJSON MSG

main :: IO ()
main = do
  bs <- B.readFile "src/Demo/JSON/private_message.json"
  print bs
  case eitherDecodeStrict' bs of
    Left e -> error e
    Right r -> print $ command r

  let value = bs
  print $ bs ^.. values.key "command"._String
