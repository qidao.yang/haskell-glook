{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Database where

import           Control.Monad.Logger        (runStderrLoggingT)
import           Control.Monad.Reader
import           Data.Pool
import           Data.Text
import           Database.Persist.Postgresql
import           Database.Persist.Sqlite
import           Yesod
import           Data.Time
import           Data.Ratio

import Enums

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
  Car json
    make String

  Person json
    name String
    address String Maybe
    carId CarId Maybe

  Job json
    title String
    description String Maybe
    deriving

  User json
    union_id String Maybe
    open_id String Maybe
    app_id String Maybe
    head_url String Maybe
    nickname String Maybe
    code String Maybe
    introduction String Maybe
    create_date UTCTime default=CURRENT_TIME
    update_date UTCTime default=CURRENT_TIME
    deriving Show

  Group json
    user_id UserId
    code String Maybe
    lable Int Maybe
    introduction String Maybe
    hear_url String Maybe
    price Rational Maybe
    create_date UTCTime default=CURRENT_TIME
    update_date UTCTime default=CURRENT_TIME
    deriving Show

  Account json
    user_id UserId
    balance_amount Rational
    withdraw_amount Rational
    state AccountStateE
    create_date UTCTime default=CURRENT_TIME
    update_date UTCTime default=CURRENT_TIME
    deriving Show

  Account_slip json
    account_id AccountId
    type AccountSlipTypeE
    amount Rational
    create_date UTCTime default=CURRENT_TIME
    update_date UTCTime default=CURRENT_TIME
    deriving Show

  WithdrawT sql=withdraw
    account_id AccountId
    state DealTypeE
    create_date UTCTime default=CURRENT_TIME
    update_date UTCTime default=CURRENT_TIME
    deriving Show

  Transaction
    account_id AccountId
    group_id GroupId
    user_id UserId
    price Rational
    state DealTypeE
    create_date UTCTime default=CURRENT_TIME
    update_date UTCTime default=CURRENT_TIME
    deriving Show
|]

newtype Config = Config { getPool :: Pool SqlBackend }

runDb :: (MonadUnliftIO m, MonadReader Config m) => ReaderT SqlBackend m b -> m b
runDb query = do
   pool <- asks getPool
   runSqlPool query pool

makeSqlitePool :: IO (Pool SqlBackend)
makeSqlitePool = do
   p <- runStderrLoggingT $ createSqlitePool ":memory:" 10
   runSqlPool (runMigration migrateAll) p
   carId <- runSqlPool (insert $ Car "make1") p
   _ <- runSqlPool (insert $ Car "make2") p
   _ <- runSqlPool (insert $ Car "make3") p
   _ <- runSqlPool (insert $ Person "George" Nothing (Just carId)) p
   _ <- runSqlPool (insert $ Person "John" (Just "address") (Just carId)) p
   return p

makeSqlitePoolFromFile :: String -> IO (Pool SqlBackend)
makeSqlitePoolFromFile fn = do
  p <- runStderrLoggingT $ createSqlitePool (pack fn) 10
  runSqlPool (runMigration migrateAll) p
  return p

makePostgresPool :: Bool -> IO (Pool SqlBackend)
makePostgresPool doMigrate = do
  p <- runStderrLoggingT $ createPostgresqlPool "host=localhost dbname=postgres user=postgres password=postgres port=5432" 10
  when doMigrate $ runSqlPool (runMigration migrateAll) p
  return p
