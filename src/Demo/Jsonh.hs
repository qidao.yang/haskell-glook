{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}
import GHC.Generics
import Control.Lens
import Data.Aeson
import Data.Aeson.Lens
import qualified Data.Map as Map
import qualified Data.ByteString.Lazy.Char8 as BS

data Response1 = Response1 { page :: Int
                           , fruits :: [String]
                           } deriving (Show, Generic)
instance FromJSON Response1
instance ToJSON Response1

data MSG = MSG { command :: String, body :: Value}
    deriving (Show, Generic)
instance FromJSON MSG
instance ToJSON MSG

test = do
    BS.putStrLn $ encode True
    let str2 = "{\"command\":\"private_message\",\"body\":{\"message_id\":\"A71B222B444D4D8E99091625C727EEC7\",\"robot_id\":\"0212016F7CBED9167C595A5B4D9CF245\",\"user_id\":\"E3A6D7A1C6470A7BCC94E33DE3CF1688\",\"send_time\":\"2019-04-22 10:04:29.000\",\"type\":3,\"content\":\"test \\u673a\\u5668\\u4eba\",\"title\":\"\",\"desc\":\"\",\"url\":\"\",\"voice_time\":\"0\"}}"
    print str2
    let Just res = decode str2 :: Maybe MSG
    print res
    let raw = BS.unpack $ encode res
    writeFile "src/Demo/JSON/private_message.json" raw
    writeFile "src/Demo/JSON/private_message_bit.json" $ BS.unpack str2

main = do
    BS.putStrLn $ encode True
    BS.putStrLn $ encode (1 :: Int)
    BS.putStrLn $ encode (2.34 :: Double)
    BS.putStrLn $ encode ("haskell" :: String)
    BS.putStrLn $ encode (["apple", "peach", "pear"] :: [String])
    BS.putStrLn $ encode $ Map.fromList ([("apple", 5), ("lettuce", 7)] :: [(String, Int)])
    BS.putStrLn $ encode $ Response1 {page = 1, fruits = ["apple", "peach", "pear"]}

    let byt = "{\"num\":6.13,\"strs\":[\"a\",\"b\"]}"
    let Just dat = decode byt :: Maybe Value
    print dat
    let Just num = dat ^? key "num"
    print num
    let Just str1 = dat ^? key "strs" . nth 0
    print str1

    let str = "{\"page\": 1, \"fruits\": [\"apple\", \"peach\"]}"
    let str2 = "{\"command\":\"private_message\",\"body\":{\"message_id\":\"A71B222B444D4D8E99091625C727EEC7\",\"robot_id\":\"0212016F7CBED9167C595A5B4D9CF245\",\"user_id\":\"E3A6D7A1C6470A7BCC94E33DE3CF1688\",\"send_time\":\"2019-04-22 10:04:29.000\",\"type\":3,\"content\":\"test \\u673a\\u5668\\u4eba\",\"title\":\"\",\"desc\":\"\",\"url\":\"\",\"voice_time\":\"0\"}}"
    let Just res = decode str :: Maybe Response1
    print res
    putStrLn $ (fruits res) !! 0
