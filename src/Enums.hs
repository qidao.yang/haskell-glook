{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}

module Enums where

import Database.Persist.TH
import Yesod
import GHC.Generics

data AccountStateE = Active | Frozen | Unavailable
    deriving (Show, Read, Eq, Generic)
derivePersistField "AccountStateE"
instance ToJSON AccountStateE
instance FromJSON AccountStateE

data AccountSlipTypeE = Withdraw | Purchase
 deriving (Show, Read, Eq, Generic)
derivePersistField "AccountSlipTypeE"
instance ToJSON AccountSlipTypeE
instance FromJSON AccountSlipTypeE

data DealTypeE = Ongoing | Succ | Fail
 deriving (Show, Read, Eq, Generic)
derivePersistField "DealTypeE"
instance ToJSON DealTypeE
instance FromJSON DealTypeE

