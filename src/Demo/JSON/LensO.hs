{-# LANGUAGE TemplateHaskell #-}
import Control.Lens hiding (element)

data Atom = Atom { _element :: String, _point :: Point } deriving (Show)

data Point = Point { _x :: Double, _y :: Double } deriving (Show)

makeLenses ''Atom
makeLenses ''Point

shiftAtomX :: Atom -> Atom
shiftAtomX = over (point . x) (+ 1)
