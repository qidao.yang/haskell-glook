### Referenc

1. [warp](http://www.aosabook.org/en/posa/warp.html)
2. [readerT pattern](https://www.fpcomplete.com/blog/2017/06/readert-design-pattern)
3. [readerT, writerT in a computation stack](https://blog.ssanj.net/posts/2018-01-12-stacking-the-readert-writert-monad-transformer-stack-in-haskell.html)
