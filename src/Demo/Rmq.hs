#!/usr/bin/env stack
{- stack --install-ghc
    runghc
    --package amqp
    --package bytestring
    --package text
-}
{-# LANGUAGE OverloadedStrings #-}

import Network.AMQP

import           Control.Monad (forM_)
import qualified Data.ByteString.Lazy.Char8 as BL
import qualified Data.ByteString as B
import           Data.Monoid ((<>))
import qualified Data.Text as DT
import qualified Data.Text.Encoding as DT
import           System.Environment (getArgs)

-- [rmq](https://rabbitmq.gemii.cc/)      gbotdev:XQLRFv2mTu4jbVev

-- GPET_MQ_CONFIG = {
--         'server': 'amqp://gbotdev:XQLRFv2mTu4jbVev@gbotdev.gemii.cc:2100/gbotdev',
--         'exchange_type': 'topic',
--         'exchange_name': 'gemii.gbot.topic',
--     }

logsExchange = "gemii.gbot.topic"



main :: IO ()
main = do
     conn       <- openConnection''  $ fromURI "amqp://gbotdev:XQLRFv2mTu4jbVev@gbotdev.gemii.cc:2100/gbotdev"
     ch         <- openChannel conn
     severities <- getArgs

     -- declareExchange ch newExchange {exchangeName    = logsExchange,
     --                                 exchangeType    = "topic",
     --                                 exchangeDurable = True}
     (q, _, _) <- declareQueue ch newQueue {queueName       = "queue.bot.msg.pet"}
                                            -- queueAutoDelete = True,
                                            -- queueDurable    = True}
     -- forM_ severities (bindQueue ch "queue.bot.msg.pet" logsExchange . DT.pack)
     bindQueue ch q logsExchange (DT.pack "queue.bot.msg.pet.test")

     BL.putStrLn " [*] Waiting for messages. To exit press CTRL+C"
     _ <- consumeMsgs ch q Ack deliveryHandler

     -- waits for keypresses
     getLine
     closeConnection conn

deliveryHandler :: (Message, Envelope) -> IO ()
deliveryHandler (msg, metadata) = do
  B.putStrLn $ "[x]" <> ( DT.encodeUtf8 $ DT.pack $ show body)
  BL.putStrLn " [x] Done"
  ackEnv metadata
  where
    body = msgBody msg
    key  = BL.fromStrict . DT.encodeUtf8 $ envRoutingKey metadata
